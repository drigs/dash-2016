﻿using UnityEngine;
using System.Collections;
using FrozenCore;

[RequireComponent(typeof(BoxCollider))]
public abstract class LevelManager : MonoBehaviour {

    private static LevelManager instance;
    public static LevelManager Instance { get { return instance; } }

    [SerializeField]
    protected float detectionTimerDuration;

    [SerializeField]
    //Light[] redLights;

    private void Awake()
    {
        instance = this;
    }

    public void LevelCompleted(int level)
    {
        PlayerPrefs.SetInt("levelCompleted", level);

        Player.Instance.enabled = false;

        ScenesManager.Instance.LoadScene("MissionSelect", ScenesManager.SceneTransition.Fade);

        AudioManager.Instance.PlayOnce("victory", AudioLayer.Ambient);
    }

    public void LevelFailure(int level)
    {
        Player.Instance.enabled = false;

        PlayerPrefs.SetInt("levelFailed", level);

        ScenesManager.Instance.LoadScene("MissionSelect", ScenesManager.SceneTransition.Fade);
    }

    //public void TurnOnLights()
    //{
    //    foreach (var light in redLights)
    //    {
    //        light.enabled = true;
    //    }
    //}

    //public void TurnOffLights()
    //{
    //    foreach (var light in redLights)
    //    {
    //        light.enabled = false;
    //    }
    //}

    public abstract void PlayerDetected();
    public abstract void PlayerEscaped();
}
