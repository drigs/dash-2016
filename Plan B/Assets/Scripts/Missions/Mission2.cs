﻿using UnityEngine;
using System.Collections;
using System;

using FrozenCore;

public class Mission2 : LevelManager
{
    [SerializeField]
    private Transform buttonDoorPivot;

    [SerializeField]
    private Transform objectiveDoorPivot;

    public override void PlayerDetected()
    {
        if (Player.Instance.Detected == false)
        {
            Tween openButtonDoor = new Tween(Vector3.zero, new Vector3(0, -80, 0), 2, false, EasingType.EaseOut, delegate (Vector3 interpolatedValue)
            {
                buttonDoorPivot.localEulerAngles = interpolatedValue;
            });

            Tween closeObjectiveDoor = new Tween(new Vector3(0, -80, 0), Vector3.zero, 1, false, EasingType.EaseOut, delegate (Vector3 interpolatedValue)
            {
                objectiveDoorPivot.localEulerAngles = interpolatedValue;
            });

            Player.Instance.StartTimer(detectionTimerDuration);
        }
    }

    public override void PlayerEscaped()
    {

    }
}
