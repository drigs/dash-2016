﻿using UnityEngine;
using System.Collections;
using System;

using FrozenCore;

public class Mission1 : LevelManager {

    [SerializeField]
    private Transform emergencyDoorPivot;

    [SerializeField]
    private Transform objectiveDoorPivot;

    public override void PlayerDetected()
    {
        if (Player.Instance.Detected == false)
        {
            emergencyDoorPivot.localEulerAngles = new Vector3(0, -80, 0);

            if (objectiveDoorPivot.localEulerAngles.y != 0)
            {
                Tween openButtonDoor = new Tween(new Vector3(0, -80, 0), Vector3.zero, 0.5f, false, EasingType.EaseOut, delegate (Vector3 interpolatedValue)
                {
                    objectiveDoorPivot.localEulerAngles = interpolatedValue;
                });
            }

            Player.Instance.StartTimer(detectionTimerDuration);
            //GameManager.Instance.SoundAlarm();
            //TurnOnLights();
        }
    }

    public override void PlayerEscaped()
    {
        
    }
}
