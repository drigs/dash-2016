﻿using UnityEngine;
using System.Collections;
using System;

using FrozenCore;

public class Mission0 : LevelManager
{
    [SerializeField]Drone[] drones;
    [SerializeField]Light[] lights;

    public override void PlayerDetected()
    {
        GameManager.Instance.SoundAlarm();
        GameManager.Instance.SendDrones(drones);

        foreach (var light in lights)
        {
            light.enabled = true;
        }
    }

    public override void PlayerEscaped()
    {
        GameManager.Instance.StopAlarm();
        Player.Instance.StopTimer();

        foreach (var light in lights)
        {
            light.enabled = false;
        }
    }
}
