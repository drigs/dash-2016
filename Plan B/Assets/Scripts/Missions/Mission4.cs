﻿using UnityEngine;
using System.Collections;
using System;

using FrozenCore;

public class Mission4 : LevelManager
{
    private bool isSpecificDoorOpen = false;

    //public override void PlayerDetected()
    //{
    //    if (Player.Instance.Detected == false)
    //    {
    //        Player.Instance.StartTimer(detectionTimerDuration);
    //    }        
    //}

    public override void PlayerDetected()
    {
        //GameManager.Instance.SoundAlarm();
        //GameManager.Instance.SendDrones(FindObjectsOfType<Drone>());

        if (Player.Instance.Detected == false)
        {
            Player.Instance.StartTimer(detectionTimerDuration);
        }      
    }

    public override void PlayerEscaped()
    {
        GameManager.Instance.StopAlarm();
        Player.Instance.StopTimer();
    }
}
