﻿using UnityEngine;
using System.Collections;
using System;

using FrozenCore;

public class Mission3 : LevelManager
{
    [SerializeField]
    private Transform objectiveDoorPivot;

    private bool isSpecificDoorOpen = false;

    public override void PlayerDetected()
    {
        if (Player.Instance.Detected == false)
        {
            if(objectiveDoorPivot.localEulerAngles.y != 0)
            {
                Tween openButtonDoor = new Tween(new Vector3(0, -80, 0), Vector3.zero, 1, false, EasingType.EaseOut, delegate (Vector3 interpolatedValue)
                {
                    objectiveDoorPivot.localEulerAngles = interpolatedValue;
                });

                openButtonDoor.onComplete += delegate
                {
                    isSpecificDoorOpen = false;
                };
            }

            Player.Instance.StartTimer(detectionTimerDuration);
        }
    }

    public override void PlayerEscaped()
    {

    }

    public void OpenSpecificDoor()
    {
        if(isSpecificDoorOpen == false)
        {
            isSpecificDoorOpen = true;

            Tween openButtonDoor = new Tween(Vector3.zero, new Vector3(0, -80, 0), 1, false, EasingType.EaseOut, delegate (Vector3 interpolatedValue)
            {
                objectiveDoorPivot.localEulerAngles = interpolatedValue;
            });
        }
    }
}
