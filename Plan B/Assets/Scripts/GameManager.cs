﻿using UnityEngine;
using System.Collections;
using FrozenCore;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    [SerializeField]
    private LevelManager currentLevel;

    NavMeshAgent[] drones;
    GameObject[] securityCams;

    public event System.Action OnUpdate;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        var droneObjs = GameObject.FindGameObjectsWithTag("Drone");

        if (currentLevel == null)
        {
            currentLevel = FindObjectOfType<LevelManager>();
        }

        drones = new NavMeshAgent[droneObjs.Length];

        for (int i = 0; i < droneObjs.Length; i++)
        {
            drones[i] = droneObjs[i].GetComponent<NavMeshAgent>();
        }
    }

    void Update()
    {
        if (OnUpdate != null)
        {
            OnUpdate();
        }
    }

    public void LaserHit()
    {
        Debug.Log("Hit laser!");      

        currentLevel.PlayerDetected();
    }

    public void SoundAlarm()
    {
        AudioSource audioSource = GetComponent<AudioSource>();

        if (!audioSource.isPlaying)
        {
            audioSource.clip = AudioManager.Instance.GetClip("alarm");
            audioSource.Play();
        }
    }

    public void StopAlarm()
    {
        var audioSource = GetComponent<AudioSource>();
        if (audioSource.clip == AudioManager.Instance.GetClip("alarm"))
        {
            GetComponent<AudioSource>().Stop();
        }
    }

    public void SendDrones(Drone[] drones)
    {
        foreach (var drone in drones)
        {
            drone.Initialize();
        }
    }

    public void StopDrones(Drone[] drones)
    {
        foreach (var drone in drones)
        {
            drone.Stop();
        }
    }

    public void CheckDrones()
    {
        foreach (var drone in FindObjectsOfType<Drone>())
        {
            if (drone.GetComponent<Drone>().isActive)
            {
                return;
            }
        }

        LevelManager.Instance.PlayerEscaped();
        Debug.Log("Player escaped");
    }

    public void TriggerCamera()
    {
        Debug.Log("Seen by camera!");
        currentLevel.PlayerDetected();
    }
}
