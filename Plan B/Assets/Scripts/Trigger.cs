﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;
using FrozenCore;

public class Trigger : Highlightable
{
    public bool repeatable;
    public UnityEvent onClick;
    public UnityEvent onCollision;
    public LayerMask collisionMask = Physics.AllLayers;
    
    public override void OnMouseDown()
    {
        if (isHighlighted)
        {
            if (onClick.GetPersistentEventCount() > 0)
            {
                onClick.Invoke();

                if (!repeatable)
                {
                    Destroy(this);
                }
            }
        }
    }

    public override void OnMouseEnter()
    {
        if (onClick.GetPersistentEventCount() > 0)
        {
            base.OnMouseEnter();
        }
    }

    public override void OnMouseOver()
    {
        if (onClick.GetPersistentEventCount() > 0)
        {
            base.OnMouseOver();
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (onCollision != null)
        {
            if (collisionMask == (collisionMask | (1 << collider.gameObject.layer)))
            {
                onCollision.Invoke();

                if (!repeatable)
                {
                    onCollision = null;
                }
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (onCollision != null)
        {
            if (collisionMask == (collisionMask | (1 << collision.gameObject.layer)))
            {
                onCollision.Invoke();

                if (!repeatable)
                {
                    onCollision = null;
                }
            }
        }
    }

    //public void ShowText(string text)
    //{
    //    GameManager.Instance.ShowText(text, transform);
    //}
}
