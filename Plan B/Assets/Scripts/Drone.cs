﻿using UnityEngine;
using System.Collections;
using FrozenCore;

public class Drone : MonoBehaviour
{
    public bool isActive { get; private set; }

    [SerializeField]Transform[] helices;
    [SerializeField]AudioClip cameraSound;
    [SerializeField]AudioClip detectionSound;

    [SerializeField]
    Vector3 northPoint = new Vector3(-14, 3.4f, 5);

    [SerializeField]
    Vector3 southPoint = new Vector3(-14, 3.4f, -15);

    NavMeshAgent agent;
    AudioSource audioSource;

    Vector3 initialPos;
    Vector3 initialLookDir;

    Tween timer;
    bool returning;
    bool foundPlayer;
    bool patrol = true;

    void Start()
    {
        //foreach (var helice in helices)
        //{
        //    Timer rotTimer = new Timer(.5f, delegate
        //    {
        //        helice.Rotate(Vector3.up * 90);
        //    });
        //    rotTimer.IsLoop();
        //}
        audioSource = GetComponent<AudioSource>();
        agent = GetComponentInParent<NavMeshAgent>();
        agent.SetDestination(southPoint);
    }

    public void Initialize()
    {
        agent.SetDestination(Player.Instance.transform.position);
        audioSource.PlayOneShot(cameraSound);

        initialPos = transform.position;
        initialLookDir = transform.eulerAngles;

        isActive = true;
        returning = false;
        foundPlayer = false;
        patrol = false;
    }

    void Update()
    {
        foreach (var helice in helices)
        {
            helice.Rotate(Vector3.up * 45);
        }

        if(patrol)
        {
            if (Vector3.Distance(agent.transform.position, Player.Instance.transform.position) <= 3)
            {
                patrol = false;
                //Mission4.Instance.PlayerDetected();
                Initialize();
            }

            if (Vector3.Distance(agent.transform.position, northPoint) <= 2)
            {
                agent.SetDestination(southPoint);
            }
            else if(Vector3.Distance(agent.transform.position, southPoint) <= 2)
            {
                agent.SetDestination(northPoint);
            }
        }
        else
        {
            if (isActive)
            {
                Vector3 playerPos = Player.Instance.transform.position;

                if (!returning)
                {
                    NavMeshPath path = new NavMeshPath();

                    if (agent.CalculatePath(playerPos, path))
                    {
                        agent.SetDestination(playerPos);
                    }
                    else
                    {
                        Player.Instance.StopTimer();

                        Timer timer = new Timer(5f, delegate
                        {
                            agent.SetDestination(initialPos);
                            agent.stoppingDistance = 0;
                            //returning = true;
                            patrol = true;
                            agent.SetDestination(northPoint);
                        });
                    }

                    if (foundPlayer)
                    {
                        if (Vector3.Distance(transform.position, playerPos) > 3)
                        {
                            Player.Instance.StopTimer();
                            foundPlayer = false;
                            audioSource.Stop();
                            audioSource.PlayOneShot(cameraSound);
                        }
                    }
                    else if (Vector3.Distance(transform.position, playerPos) < 3)
                    {
                        audioSource.PlayOneShot(detectionSound);
                        foundPlayer = true;
                        Player.Instance.StartTimer(2);
                    }
                }
                else
                {
                    if (Vector3.Distance(transform.position, playerPos) < 20)
                    {
                        returning = false;
                    }
                    else if (Vector3.Distance(transform.position, initialPos) < 1)
                    {
                        returning = false;
                        Stop();
                    }
                }
            }
        }
    }

    public void Stop()
    {
        //agent.Stop();
        isActive = false;
        GameManager.Instance.CheckDrones();
    }
}
