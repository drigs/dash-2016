﻿using UnityEngine;
using System.Collections;
using FrozenCore;

public class SecurityCamera : MonoBehaviour
{
    [SerializeField]
    private float visionAngle;

    [SerializeField]
    private float freezingTime;

    void Start()
    {
        System.Action RotateCam = null;
        
        float initialY = transform.eulerAngles.y;
        float targetY = transform.eulerAngles.y + visionAngle;

        RotateCam = delegate
        {
            Timer loopTimer = new Timer(2, delegate
            {
                Tween camRot = new Tween(initialY, targetY, 5, false, EasingType.Linear, delegate (float value)
                {
                    Vector3 rot = transform.eulerAngles;
                    rot.y = value;
                    transform.eulerAngles = rot;
                });

                camRot.onComplete = delegate
                {
                    Timer loopReturnTimer = new Timer(2, delegate
                    {
                        Tween camReturn = new Tween(targetY, initialY, 5, false, EasingType.Linear, delegate (float value)
                        {
                            Vector3 rot = transform.eulerAngles;
                            rot.y = value;
                            transform.eulerAngles = rot;
                        });
                        camReturn.onComplete = RotateCam;
                    });
                };
            });
        };
        RotateCam();
    }
}
