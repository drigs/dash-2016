﻿using UnityEngine;
using System.Collections;
using FrozenCore;

public enum Axis
{
    X, Y, Z
}

public class Laser : MonoBehaviour
{
    public float maxDistance = 2;
    public float travelTime = 5;
    public Axis axis;

    void Start()
    {
        System.Action Translate = null;

        float initValue = 0;

        switch (axis)
        {
            case Axis.X:
                initValue = transform.position.x;
                break;
            case Axis.Y:
                initValue = transform.position.y;
                break;
            case Axis.Z:
                initValue = transform.position.z;
                break;
            default:
                break;
        }

        float targetValue = initValue + maxDistance;

        Translate = delegate
        {
            Tween moveTween = new Tween(initValue, targetValue, travelTime / 2, false, EasingType.Linear, SetAxisValue);

            moveTween.onComplete = delegate
            {
                Tween returnTween = new Tween(targetValue, initValue, travelTime / 2, false, EasingType.Linear, SetAxisValue);

                returnTween.onComplete = Translate;
            };            
        };

        Translate();
    }

    void SetAxisValue(float value)
    {
        var newPos = transform.position;

        switch (axis)
        {
            case Axis.X:
                newPos.x = value;
                break;
            case Axis.Y:
                newPos.y = value;
                break;
            case Axis.Z:
                newPos.z = value;
                break;
            default:
                break;
        }

        transform.position = newPos;
    }
}
