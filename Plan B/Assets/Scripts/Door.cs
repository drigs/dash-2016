﻿using UnityEngine;
using System.Collections;
using FrozenCore;

public class Door : Trigger {

    #region Attributes
    [Header("Door Variables")]
    [SerializeField]
    private int keyIndex;
    public int KeyIndex { get { return keyIndex; } }

    [SerializeField]
    private Transform targetTransform;
    public Transform TargetTransform { get { return targetTransform; } }

    private Tween openTween;
    #endregion

    #region Methods
    private new void Awake()
    {
        base.Awake();

        onClick.AddListener(Open);
    }

    private void Open()
    {

    }
    #endregion
}
