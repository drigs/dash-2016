﻿using UnityEngine;
using System.Collections;
using FrozenCore;

public class Looker : MonoBehaviour
{
    [SerializeField]
    float minDistance = 5;
    [SerializeField]
    float lookSpeed = 1;
    Vector3 initialLookDir;

    void Awake()
    {
        initialLookDir = transform.forward;
    }

    void Update()
    {
        if (Vector3.Distance(transform.position, Player.Instance.transform.position) <= minDistance)
        {
            Vector3 lookDir = Vector3.RotateTowards(transform.forward, transform.position - Camera.main.transform.position, lookSpeed * Time.deltaTime, Mathf.PI / 2);
            lookDir.y = 0;

            if (Vector3.Angle(transform.parent.forward, lookDir) < 90)
            {
                transform.rotation = Quaternion.LookRotation(lookDir);
            }
        }
        else
        {
            if (transform.forward != initialLookDir)
            {
                Vector3 lookDir = Vector3.RotateTowards(transform.forward, initialLookDir, Time.deltaTime, 0);
                transform.rotation = Quaternion.LookRotation(lookDir);
            }
        }
    }

}
