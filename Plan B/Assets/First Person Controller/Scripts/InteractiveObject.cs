﻿using UnityEngine;

public abstract class InteractiveObject : MonoBehaviour
{
    public abstract void OnMouseEnter();

    public abstract void OnMouseOver();

    public abstract void OnMouseExit();

    public abstract void OnMouseDown();
}