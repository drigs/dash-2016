﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using FrozenCore;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    #region References

    [SerializeField]AudioClip[] footsteps;
    [SerializeField]Transform hand;
    [SerializeField]LayerMask interactionMask;

    [SerializeField]
    private CanvasGroup timerGroup;

    [SerializeField]
    private Text timerText;

    private bool[] keysBag;

    [SerializeField]
    private bool inLobby;
    #endregion

    #region Attributes
    public static Player Instance { get; private set; }

    public Transform Hand { get { return hand; } }    

    public float radius { get; private set; }
    public float height { get; private set; }
    public float speed = 5f;
    public float runSpeed = 10f;
    public float turnSpeed = 2f;
    public float interactionDistance = 5f;
    public float zoomAmount = 25;

    public bool lockedForward { get; set; }

    public RectTransform cursor;
    public Transform[] cameraPoints;

    public Transform palmtop;
    public Transform pocket;
    public Transform secondaryHand;
    public GameObject menuScreen;
    public GameObject handScreen;

    public Light redLight;

    float initialFov;
    float initialCamHeight;
    float stepCycle;
    float nextStep;
    
    bool canMove = true;
    bool canRotate = true;
    bool canUnlock = true;

    bool zooming;
    bool running;
    bool crouching;
    bool palmIsUp;

    CharacterController cc;

    BoxCollider boxChecker;

    AudioSource audioSource;
    
    Action specialAction;
    Action onActionEnd;

    public Tween tickTimer;

    bool detected = false;
    public bool Detected { get { return detected; } }
    #endregion

    #region Methods
    void Initialize()
    {
        Instance = this;

        audioSource = GetComponent<AudioSource>();

        cc = GetComponentInChildren<CharacterController>();

        keysBag = new bool[2];

        redLight = GetComponentInChildren<Light>();

        height = cc.height;
        radius = cc.radius;

        initialFov = Camera.main.fieldOfView;
        initialCamHeight = Camera.main.transform.localPosition.y;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        if (inLobby == true)
        {
            canRotate = false;
            canMove = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            cursor.gameObject.SetActive(false);
        }
        else
        {
            Scene screen = SceneManager.GetActiveScene();
            char sceneIndex = screen.name[screen.name.Length - 1];
            handScreen.GetComponentInChildren<Text>().text = "MISSION " + sceneIndex + "\n\nExecuting Plan A";
        }
    }

    void Awake()
    {
        Initialize();
    }

    void FixedUpdate()
    {
        if (cc.enabled) cc.Move(Vector3.down * Time.fixedDeltaTime);

        if (canMove)
        {
            //Movement
            float xMove = Input.GetAxisRaw("Horizontal");
            float zMove = Input.GetAxisRaw("Vertical");

            if (Mathf.Abs(xMove) > 0 || Mathf.Abs(zMove) > 0)
            {
                Vector3 movement = (transform.right * xMove + transform.forward * zMove).normalized;
                
                RaycastHit hitInfo;
                Physics.SphereCast(transform.position, cc.radius * 2, Vector3.down, out hitInfo, cc.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);

                movement = Vector3.ProjectOnPlane(movement, hitInfo.normal).normalized;

                Vector3 direction = movement.normalized;

                movement = direction * (running ? runSpeed : (crouching ? speed / 2 : speed)) * Time.fixedDeltaTime;
                movement.y -= .1f;
                
                Walk(movement);
            }
        }
    }

    void Update()
    {
        if (canRotate)
        {
            //Rotation
            float xRot = Input.GetAxis("Mouse Y") * turnSpeed;
            float yRot = Input.GetAxis("Mouse X") * turnSpeed;

            Quaternion camRot0 = Camera.main.transform.localRotation;
            Quaternion playerRot0 = transform.localRotation;

            Quaternion cameraRot = Camera.main.transform.localRotation * Quaternion.Euler(-xRot, 0, 0);
            Quaternion playerRot = transform.localRotation * Quaternion.Euler(0, yRot, 0);

            cameraRot = ClampRotationAroundXAxis(cameraRot);

            transform.localRotation = playerRot;
            Camera.main.transform.localRotation = cameraRot;
        }

        if (!inLobby)
        {
            CheckInput();
        }

        if (specialAction != null)
        {
            specialAction();
        }
    }

    public void SetLock(bool value)
    {
        canUnlock = !value;
    }

    public void SetMove(bool value)
    {
        cc.enabled = canMove = value;
    }

    public void SetRotate(bool value)
    {
        canRotate = value;
        cursor.gameObject.SetActive(value);
    }

    public void Quit()
    {
        Application.Quit();
    }

    void Walk(Vector3 movement)
    {
        if (lockedForward ? (movement.z >= -.01f) : true)
        {
            Vector3 previousPos = transform.position;

            cc.Move(movement);

            stepCycle += (cc.velocity.magnitude + speed) * Time.fixedDeltaTime;

            //if (Vector3.Distance(previousPos, transform.position) > .05f)
            {
                if (!(stepCycle > nextStep))
                {
                    return;
                }

                nextStep = stepCycle + 3.5f;

                int n = UnityEngine.Random.Range(1, footsteps.Length);

                audioSource.clip = footsteps[n];
                audioSource.pitch = UnityEngine.Random.Range(.85f, 1.15f);
                audioSource.PlayOneShot(audioSource.clip);

                footsteps[n] = footsteps[0];
                footsteps[0] = audioSource.clip;
            }
        }
    }
    
    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        angleX = Mathf.Clamp(angleX, -60, 60);

        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }

    void CheckInput()
    {
        running = (Input.GetKey(KeyCode.LeftShift)) && !crouching;

        if (!Input.GetKey(KeyCode.LeftControl))
        {
            if (crouching)
            {
                if (!Physics.Raycast(transform.position, transform.up, height))
                {
                    cc.height = height;
                    Tween camTween = new Tween(Camera.main.transform.localPosition.y, initialCamHeight, .25f, false, EasingType.Linear, delegate (float value)
                    {
                        Vector3 camPos = Camera.main.transform.localPosition;
                        camPos.y = value;
                        Camera.main.transform.localPosition = camPos;

                        Vector3 pos = transform.position;
                        pos.y = value + .2f;
                        transform.position = pos;
                    });

                    camTween.DestroyOnComplete = true;
                    crouching = false;
                }
            }
        }
        else
        {
            if (!crouching)
            {
                cc.height = height * 0.35f;
                Tween camTween = new Tween(Camera.main.transform.localPosition.y, initialCamHeight / 2, .25f, false, EasingType.Linear, delegate (float value)
                {
                    Vector3 camPos = Camera.main.transform.localPosition;
                    camPos.y = value;
                    Camera.main.transform.localPosition = camPos;

                    Vector3 pos = transform.position;
                    pos.y = value + .2f;
                    transform.position = pos;
                });

                camTween.DestroyOnComplete = true;
                crouching = true;
            }
        }        

        if (!Input.GetMouseButton(1))
        {
            if (zooming)
            {
                Tween cameraZoom = new Tween(Camera.main.fieldOfView, initialFov, .25f, false, EasingType.EaseOut, delegate (float value)
                {
                    Camera.main.fieldOfView = value;
                });

                cameraZoom.DestroyOnComplete = true;
                zooming = false;
            }
        }
        else
        {
            if (!zooming)
            {
                Tween cameraZoom = new Tween(Camera.main.fieldOfView, initialFov - 25, .25f, false, EasingType.EaseOut, delegate (float value)
                {
                    Camera.main.fieldOfView = value;
                });

                cameraZoom.DestroyOnComplete = true;
                zooming = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (canUnlock)
            {
                if (onActionEnd != null)
                {
                    onActionEnd();
                    onActionEnd = null;
                }

                specialAction = null;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (palmIsUp)
            {
                //if (palmtop.position == hand.position)
                {
                    PutAwayPalmtop();
                }
            }
            else
            {
                PickUpPalmtop();
            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (palmIsUp)
            {
                PutAwayPalmtop();
            }
            else
            {
                ShowPlans();
            }
        }
    }

    public bool CheckInteractionDistance(Vector3 target)
    {
        return Vector3.Distance(transform.position, target) <= interactionDistance;
    }

    public void StartTimer(float duration)
    {
        if(detected == false)
        {
            detected = true;

            Tween timerFadeIn = new Tween(0, 1, 1, false, EasingType.Linear, delegate (float alpha)
            {
                timerGroup.alpha = alpha;
            });

            tickTimer = new Tween(duration, 0, duration, false, EasingType.Linear, delegate (float remainingTime)
            {
                timerText.text = remainingTime.ToString("00.00");
            });

            tickTimer.onComplete += delegate
            {
                LevelManager.Instance.LevelFailure(1);
            };

            Text screenText = handScreen.GetComponentInChildren<Text>();
            Scene screen = SceneManager.GetActiveScene();
            char sceneIndex = screen.name[screen.name.Length - 1];
            screenText.text = "MISSION " + sceneIndex + "\n\nSwitching to Plan B...";

            ShowPlans();

            Timer switchTimer = new Timer(3, delegate
            {
                screenText.text = "MISSION " + sceneIndex + "\n\nExecuting Plan B";
            });

            Timer hidePalmTop = new Timer(6, delegate
            {
                PutAwayPalmtop();
            });

            redLight.enabled = true;
            GameManager.Instance.SoundAlarm();
        }
    }

    public void StopTimer()
    {
        if (detected)
        {
            detected = false;

            tickTimer.StopTween();

            Tween timerFadeOut = new Tween(1, 0, 1, false, EasingType.Linear, delegate (float alpha)
            {
                timerGroup.alpha = alpha;
            });
            
            redLight.enabled = false;
            GameManager.Instance.StopAlarm();
        }
    }

    #region Special Actions

    void OnTriggerEnter(Collider col)
    {
        switch (col.gameObject.tag)
        {
            case "Footprint":
                col.GetComponent<Renderer>().enabled = true;
                break;
            default:
                break;
        }
    }

    public void GoToLobby()
    {
        ScenesManager.Instance.LoadScene("MissionSelect", ScenesManager.SceneTransition.Fade);
    }

    public void PickUpPalmtop()
    {
        menuScreen.SetActive(true);
        handScreen.SetActive(false);

        Tween moveTween = new Tween(palmtop.position, hand.position, .25f, false, EasingType.EaseOut, delegate (Vector3 pos)
        {
            palmtop.position = pos;
        });

        Tween rotTween = new Tween(palmtop.rotation, hand.rotation, .25f, false, EasingType.Linear, delegate (Quaternion rot)
        {
            palmtop.rotation = rot;
        });

        palmIsUp = true;
        SetMove(false);
        SetRotate(false);

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        
        cursor.gameObject.SetActive(false);

        palmtop.transform.SetParent(hand);
    }

    public void PutAwayPalmtop()
    {
        Tween putAwayTween = new Tween(palmtop.position, pocket.position, .5f, false, EasingType.EaseOut, delegate (Vector3 pos)
        {
            palmtop.position = pos;
        });

        palmIsUp = false;

        if(inLobby == false)
        {
            SetMove(true);
            SetRotate(true);
        }

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        cursor.gameObject.SetActive(true);

        palmtop.transform.SetParent(pocket);

        specialAction = null;
    }

    void ShowPlans()
    {
        menuScreen.SetActive(false);
        handScreen.SetActive(true);

        specialAction += delegate
        {
            if (palmtop.position != secondaryHand.position)
            {
                palmtop.position = Vector3.Lerp(palmtop.position, secondaryHand.position, Time.deltaTime * 5f);
            }

            if (palmtop.eulerAngles != secondaryHand.eulerAngles)
            {
                palmtop.rotation = Quaternion.Lerp(palmtop.rotation, secondaryHand.rotation, Time.deltaTime * 5f);
            }
        };

        palmtop.transform.SetParent(secondaryHand);

        palmIsUp = true;
    }

    public void OpenDoor(Transform doorPivot)
    {
        Tween open = new Tween(doorPivot.eulerAngles, doorPivot.eulerAngles - Vector3.up * 80, 2f, false, EasingType.EaseOut, delegate (Vector3 rot)
        {
            doorPivot.eulerAngles = rot;
        });

        AudioManager.Instance.PlayOnce("door", AudioLayer.FX, UnityEngine.Random.Range(.85f, 1.15f));
    }

    public void CloseDoor(Transform doorPivot)
    {
        specialAction += () => CloseLerp(doorPivot);
    }

    void CloseLerp(Transform doorPivot)
    {
        if (Quaternion.Angle(doorPivot.rotation, Quaternion.identity) > 1)
        {
            doorPivot.rotation = Quaternion.Lerp(doorPivot.rotation, Quaternion.identity, 5 * Time.deltaTime);
        }
        else
        {
            Trigger trigger = doorPivot.GetComponentInChildren<Trigger>();
            if (trigger) trigger.highlightable = true;

            specialAction = null;
        }
    }
    
    public void SitDown(Transform target)
    {
        transform.position = target.position;
        transform.rotation = target.rotation;

        SetMove(false);
        SetRotate(false);

        onActionEnd = GetUp;
    }

    public void GetUp()
    {
        SetLock(false);
        SetMove(true);
        SetRotate(true);
    }

    public void TeleportTo(Transform target)
    {
        Vector3 pos = transform.position;
        pos.x = target.position.x;
        transform.position = pos;
    }
    
    public void PickUpKey(int keyIndex)
    {
        keysBag[keyIndex] = true;
        AudioManager.Instance.PlayOnce("keys", AudioLayer.FX);
    }

    public void UseKeyOnDoor(Door door)
    {
        if (keysBag[door.KeyIndex] == true)
        {
            keysBag[door.KeyIndex] = false;

            OpenDoor(door.TargetTransform);
        }
    }
    #endregion
    #endregion

    #region Unity Methods
    private void OnControllerColliderHit(ControllerColliderHit other)
    {
        if(other.collider.tag == "Objective")
        {
            LevelManager.Instance.LevelCompleted(1);
        }
    }    
    #endregion
}
