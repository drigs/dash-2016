﻿using UnityEngine;
using System.Collections;
using System;

public abstract class Highlightable : InteractiveObject
{
    [SerializeField]
    Color highlightColor = new Color(.1f, .1f, .1f);
    
    MeshRenderer[] meshRenderers;
    
    Color[] normalColors;

    protected bool isHighlighted { get; private set; }

    public bool highlightable { get; set; } 

    protected void Awake()
    {
        highlightable = true;

        meshRenderers = GetComponentsInChildren<MeshRenderer>();

        normalColors = new Color[meshRenderers.Length];

        for (int i = 0; i < meshRenderers.Length; i++)
        {
            meshRenderers[i].material.EnableKeyword("_EMISSION");
            normalColors[i] = meshRenderers[i].material.GetColor("_EmissionColor");
        }

        gameObject.layer = LayerMask.NameToLayer("Interactive");
    }

    void Update()
    {
        if (isHighlighted)
        {
            if (!Player.Instance.CheckInteractionDistance(transform.position))
            {
                Unhighlight();
            }
        }
    }

    protected void Highlight()
    {
        for (int i = 0; i < meshRenderers.Length; i++)
        {
            meshRenderers[i].material.SetColor("_EmissionColor", highlightColor);
        }

        isHighlighted = true;
    }

    protected void Unhighlight()
    {
        for (int i = 0; i < meshRenderers.Length; i++)
        {
            meshRenderers[i].material.SetColor("_EmissionColor", normalColors[i]);
        }

        isHighlighted = false;
    }

    public override void OnMouseEnter()
    {
        if (highlightable)
        {
            if (!isHighlighted)
            {
                if (Player.Instance.CheckInteractionDistance(transform.position))
                {
                    Highlight();
                }
            }
        }
    }

    public override void OnMouseOver()
    {
        if (isActiveAndEnabled)
        {
            if (highlightable)
            {
                if (!isHighlighted)
                {
                    if (Player.Instance.CheckInteractionDistance(transform.position))
                    {
                        Highlight();
                    }
                }
            }
        }
    }

    public override void OnMouseExit()
    {
        if (isHighlighted)
        {
            Unhighlight();
        }
    }

    void OnDisable()
    {
        Unhighlight();
    }

}
