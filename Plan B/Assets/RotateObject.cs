﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour {

	void Update ()
    {
        Vector3 newRotation = transform.eulerAngles;

        newRotation.y += 90 * Time.deltaTime;

        transform.eulerAngles = newRotation;
	}
}
