﻿using UnityEngine;
using System.Collections;
using FrozenCore;

public class AudioTest : MonoBehaviour {

    [SerializeField]AudioClip clip;
    LayeredAudioSource source;

    void Start()
    {
        source = gameObject.AddComponent<LayeredAudioSource>();
        source.audioLayer = AudioLayer.Ambient;
    }

	public void ButtonClick()
    {
        AudioManager.Instance.PlayOnce(clip.name, AudioLayer.FX, Random.Range(0f,3f));
        AudioManager.Instance.RemoveSource(source);
    }    
}
