﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FrozenCore
{
    public class CoreManager : MonoBehaviour {

        #region Attributes
        public static CoreManager Instance { get; private set; }

        List<Timer> timers;

        TweenManager tweenManager;

        AudioManager audioManager;
        
        [SerializeField]
        ScenesManager scenesManager;

        [SerializeField]
        EasingCurves[] easingCurves;
        #endregion

        #region Methods
        void Initialize()
        {
            Instance = this;

            timers = new List<Timer>();

            scenesManager.Initialize();

            tweenManager = new TweenManager();
            tweenManager.Initialize(easingCurves);
        }

        public void AddTimer(Timer timer)
        {
            timers.Add(timer);
        }

        public void RemoveTimer(Timer timer)
        {
            timers.Remove(timer);
        }
        #endregion

        #region Unity Methods
        void Awake()
        {
            //DontDestroyOnLoad(gameObject);

            Initialize();
        }

        void Update()
        {
            tweenManager.UpdateTweenManager();

            foreach (Timer timer in timers.ToArray())
            {
                timer.Update();
            }
        }

        void FixedUpdate()
        {
            tweenManager.FixedUpdateTweenManager();
        }
        #endregion
    }
}
