﻿using UnityEngine;
using System.Collections;

namespace FrozenCore
{
    public abstract class MachineState<T>
    {
        public abstract void Enter();
        public abstract void Exit();
    }
}
