﻿using UnityEngine;
using System;

namespace FrozenCore
{
    public class Timer
    {
        #region State
        enum TimerState
        {
            Running, Paused, Complete
        }
        #endregion

        #region Attributes
        float duration;
        float elapsedTime;

        bool isLoop;
        bool destroyOnComplete;

        Action callback;

        TimerState state;
        #endregion

        #region Methods
        public Timer(float duration, Action callback)
        {
            this.duration = duration;

            this.callback += callback;

            isLoop = false;
            destroyOnComplete = true;

            CoreManager.Instance.AddTimer(this);

            Start();
        }

        void Start()
        {
            elapsedTime = 0;

            state = TimerState.Running;
        }

        public void Pause()
        {
            state = TimerState.Paused;
        }

        public void Continue()
        {
            state = TimerState.Running;
        }

        public void Update()
        {
            switch (state)
            {
                case TimerState.Running:
                    elapsedTime += Time.deltaTime;

                    if (elapsedTime >= duration)
                    {
                        state = TimerState.Complete;
                    }

                    break;
                case TimerState.Paused:
                    break;
                case TimerState.Complete:
                    if (callback != null)
                    {
                        callback();
                    }

                    if (isLoop)
                    {
                        Start();
                    }
                    else if (destroyOnComplete)
                    {
                        CoreManager.Instance.RemoveTimer(this);
                    }
                    break;
                default:
                    break;
            }
        }

        public void IsLoop()
        {
            isLoop = true;
            destroyOnComplete = false;
        }

        public void DestroyOnComplete()
        {
            destroyOnComplete = true;
            isLoop = false;
        }
        #endregion
    }
}
