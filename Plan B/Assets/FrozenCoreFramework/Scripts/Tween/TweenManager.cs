﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FrozenCore
{
    public class TweenManager
    {
        #region Attributes
        static TweenManager instance;
        public static TweenManager Instance { get { return instance; } }

        List<Tween> tweensList;
        List<Tween> fixedTweensList;

        EasingCurves[] easingCurves;
        #endregion

        #region Methods
        public void Initialize(EasingCurves[] easingCurves)
        {
            instance = this;

            this.easingCurves = easingCurves;

            tweensList = new List<Tween>();
            fixedTweensList = new List<Tween>();
        }

        public void UpdateTweenManager()
        {
            for (int i = 0; i < tweensList.Count; i++)
            {
                tweensList[i].UpdateTween();
            }
        }

        public void FixedUpdateTweenManager()
        {
            for (int i = 0; i < fixedTweensList.Count; i++)
            {
                fixedTweensList[i].UpdateTween();
            }
        }

        public void AddTween(Tween tween)
        {
            if(tween.IsFixedTween)
            {
                fixedTweensList.Add(tween);
            }
            else
            {
                tweensList.Add(tween);
            }
        }
        
        public void RemoveTween(Tween tween)
        {
            if (tween.IsFixedTween)
            {
                fixedTweensList.Remove(tween);
            }
            else
            {
                tweensList.Remove(tween);
            }
        }

        public AnimationCurve GetAnimationCurve(EasingType easing)
        {
            for (int i = 0; i < easingCurves.Length; i++)
            {
                if(easing == easingCurves[i].Type)
                {
                    return easingCurves[i].Curve;
                }
            }

            return null;
        }
        #endregion
    }
}
