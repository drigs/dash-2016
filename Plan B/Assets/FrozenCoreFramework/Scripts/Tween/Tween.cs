﻿using UnityEngine;
using System;
using System.Collections;

namespace FrozenCore
{
    public class Tween
    {
        #region TweenState
        enum TweenState
        {
            Running, Paused, Completed
        }
        #endregion
        
        #region Attributes
        float floatFrom;
        float floatTo;
        float floatInterpolatedValue;
        Action<float> floatCallback;

        Vector3 vector3From;
        Vector3 vector3To;
        Vector3 vector3InterpolatedValue;
        Action<Vector3> vector3Callback;

        Color colorFrom;
        Color colorTo;
        Color colorInterpolatedValue;
        Action<Color> colorCallback;

        Quaternion quaternionFrom;
        Quaternion quaternionTo;
        Quaternion quaternionInterpolatedValue;
        Action<Quaternion> quaternionCallback;

        float durationTime;
        float elapsedTime;

        bool isLoop;
        bool destroyOnComplete;
        bool isFixedTween;

        public Action onComplete;

        TweenState state;
        AnimationCurve curve;

        public bool IsFixedTween { get { return isFixedTween; } }

        public bool IsLoop
        {
            set
            {
                isLoop = true;
                destroyOnComplete = false;
            }
        }

        public bool DestroyOnComplete
        {
            set
            {
                destroyOnComplete = true;
                isLoop = false;
            }
        }
        #endregion

        #region Methods
        public Tween(float from, float to, float durationTime, bool isFixedTween, EasingType easing, Action<float> callback)
        {
            this.durationTime = durationTime;
            this.floatCallback = callback;

            this.floatFrom = from;
            this.floatTo = to;
            this.isFixedTween = isFixedTween;
            
            isLoop = false;
            destroyOnComplete = true;

            state = TweenState.Paused;
            curve = TweenManager.Instance.GetAnimationCurve(easing);

            TweenManager.Instance.AddTween(this);

            StartTween();
        }

        public Tween(Vector3 from, Vector3 to, float durationTime, bool isFixedTween, EasingType easing, Action<Vector3> callback)
        {
            this.durationTime = durationTime;
            this.vector3Callback = callback;

            this.vector3From = from;
            this.vector3To = to;
            this.isFixedTween = isFixedTween;

            isLoop = false;
            destroyOnComplete = false;

            state = TweenState.Paused;
            curve = TweenManager.Instance.GetAnimationCurve(easing);

            TweenManager.Instance.AddTween(this);

            StartTween();
        }

        public Tween(Color from, Color to, float durationTime, bool isFixedTween, EasingType easing, Action<Color> callback)
        {
            this.durationTime = durationTime;
            this.colorCallback = callback;

            this.colorFrom = from;
            this.colorTo = to;
            this.isFixedTween = isFixedTween;

            isLoop = false;
            destroyOnComplete = false;

            state = TweenState.Paused;
            curve = TweenManager.Instance.GetAnimationCurve(easing);

            TweenManager.Instance.AddTween(this);

            StartTween();
        }

        public Tween(Quaternion from, Quaternion to, float durationTime, bool isFixedTween, EasingType easing, Action<Quaternion> callback)
        {
            this.durationTime = durationTime;
            this.quaternionCallback = callback;

            this.quaternionFrom = from;
            this.quaternionTo = to;
            this.isFixedTween = isFixedTween;

            isLoop = false;
            destroyOnComplete = false;

            state = TweenState.Paused;
            curve = TweenManager.Instance.GetAnimationCurve(easing);

            TweenManager.Instance.AddTween(this);

            StartTween();
        }


        public void StartTween()
        {
            elapsedTime = 0;

            state = TweenState.Running;
        }

        public void PauseTween()
        {
            state = TweenState.Paused;
        }

        public void ResumeTween()
        {
            state = TweenState.Running;
        }

        public void StopTween()
        {
            elapsedTime = 0;
            state = TweenState.Paused;
            TweenManager.Instance.RemoveTween(this);
        }

        public void UpdateTween()
        {
            switch (state)
            {
                case TweenState.Running:
                    if(isFixedTween)
                    {
                        elapsedTime += Time.fixedDeltaTime;
                    }
                    else
                    {
                        elapsedTime += Time.deltaTime;
                    }

                    if(elapsedTime >= durationTime)
                    {
                        state = TweenState.Completed;

                        elapsedTime = durationTime;
                    }

                    if(floatCallback != null)
                    {
                        floatInterpolatedValue = floatFrom + ((floatTo - floatFrom) * curve.Evaluate(elapsedTime / durationTime));

                        floatCallback(floatInterpolatedValue);
                    }
                    else if(vector3Callback != null)
                    {
                        vector3InterpolatedValue = new Vector3();
                        vector3InterpolatedValue.x = vector3From.x + ((vector3To.x - vector3From.x) * curve.Evaluate(elapsedTime / durationTime));
                        vector3InterpolatedValue.y = vector3From.y + ((vector3To.y - vector3From.y) * curve.Evaluate(elapsedTime / durationTime));
                        vector3InterpolatedValue.z = vector3From.z + ((vector3To.z - vector3From.z) * curve.Evaluate(elapsedTime / durationTime));

                        vector3Callback(vector3InterpolatedValue);
                    }
                    else if (colorCallback!= null)
                    {
                        colorInterpolatedValue = new Color();
                        colorInterpolatedValue.r = colorFrom.r + ((colorTo.r - colorFrom.r) * curve.Evaluate(elapsedTime / durationTime));
                        colorInterpolatedValue.g = colorFrom.g + ((colorTo.g - colorFrom.g) * curve.Evaluate(elapsedTime / durationTime));
                        colorInterpolatedValue.b = colorFrom.b + ((colorTo.b - colorFrom.b) * curve.Evaluate(elapsedTime / durationTime));
                        colorInterpolatedValue.a = colorFrom.a + ((colorTo.a - colorFrom.a) * curve.Evaluate(elapsedTime / durationTime));

                        colorCallback(colorInterpolatedValue);
                    }
                    else if (quaternionCallback != null)
                    {
                        quaternionInterpolatedValue = new Quaternion();
                        quaternionInterpolatedValue.x = quaternionFrom.x + ((quaternionTo.x - quaternionFrom.x) * curve.Evaluate(elapsedTime / durationTime));
                        quaternionInterpolatedValue.y = quaternionFrom.y + ((quaternionTo.y - quaternionFrom.y) * curve.Evaluate(elapsedTime / durationTime));
                        quaternionInterpolatedValue.z = quaternionFrom.z + ((quaternionTo.z - quaternionFrom.z) * curve.Evaluate(elapsedTime / durationTime));
                        quaternionInterpolatedValue.w = quaternionFrom.w + ((quaternionTo.w - quaternionFrom.w) * curve.Evaluate(elapsedTime / durationTime));

                        quaternionCallback(quaternionInterpolatedValue);
                    }
                    break;
                case TweenState.Paused:
                    break;
                case TweenState.Completed:
                    if(isLoop)
                    {
                        StartTween();
                    }
                    else if(destroyOnComplete)
                    {
                        TweenManager.Instance.RemoveTween(this);
                    }
                    else
                    {
                        state = TweenState.Paused;
                    }

                    if (onComplete != null)
                    {
                        onComplete();
                    }
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}
