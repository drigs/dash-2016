﻿public enum EasingType
{
    Linear, ElasticIn, ElasticOut, ElasticInOut, LeverPull, EaseIn, EaseOut
}